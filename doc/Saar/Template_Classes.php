<?php

/* this file contains the object types that are used in the Saar theme, written as class definitions */

class PageBlock
{
  public $type; //string with values: "form", "buttons" or "image"
  public $content; //Content object or array:
  public $title; //string with title (optional)
  public $layer; //positive integer with layer information (optional)
}

class Content
{
  
}

class FormElement
{
  public $type; //HTML input type attribute content (string)
  public $name; //HTML name attribute content (string)
  public $value; //HTML value attribute content (string)
  public $placeholder; //HTML placeholder attribute content (string)
}

class FormContent extends Content
{
  public $method; //HTML form method attribute content (string)
  public $action; //HTML form action attribute content (string)
  public $name; //HTML name attribute content (string)
  public $elements; //array of FormElement objects
  public $submitValue; //HTML form submit button value (string)
  public $autocomplete; //HTML form autocomplete attribute content (boolean)
}

class JavaScriptEvent
{
  public $event;
  public $action; //the action that shall be executed on the event
}

class ButtonContent
{
  public $action; //link reference, href (string)
  public $title; //visible text in link (string)
  public $jsEvent; //JavaScriptEvent object (optional)
  public $iconURL; //URL with image to display instead of title (optional)
  public $selected; //(boolean)
}

class ButtonGroupContent
{
  public $buttons; //array of ButtonContent elements
  public $alignH; //true: align them horizontally (in the same row)
}


class ImageContent extends Content
{
  public $url; //image URL, if clicking on it should direct the user somewhere (string)
  public $source; //image source URL (string)
  public $description; //image description (string)
}

class TextContent extends Content
{
  public $text; //the text string
  public $size; //(string or null) "big" and "small" are values for big text and small text. All other values represent normal text size.
}

class LinkContent extends Content
{
  public $action; //link reference, href (string)
  public $title; //visible text in link (string)
}

class WidgetPart
{
  public $type; //string with values: "form", "buttons" or "image"
  public $content; //Content object or array:
                  //type == "form" => FormContent; type == "button" => ButtonContent; type == "image" => ImageContent, "text" => TextContent, "html" => string
  public $layer;  //positive integer with layer information (optional)
  public $isLink; //DEPRECATED! (boolean) for text content: if true, $content is included in an <a> tag, otherwise a <p> tag is used.
}

class Widget
{
  public $title; //string
  public $description; //string
  public $parts; //array of WidgetPart
  public $layer; //positive integer with layer information (optional), only relevant on main page content
  public $isSideWidget; //true: side widget, otherwise main content widget
}

?>