<?php

include ('./Content.php');

class FormElement
{
  public $type = null;
  public $value = null; //the derived classes define what kind of data will be stored in $value
  public $name = ""; //the element's name. Will be used for the name attribute
  public $title = ""; //a short text telling the user what the input field is there for
  public $description = ""; //a description for the input field (optional)
  public $jsEvents = []; //array of JsEvent instances (Definition in Content.php)
  public $hasAutofocus = false; //if the focus should be set on the element by default
  public $required = false; //if it is required to fill out the element
}

//standard HTML form elements:

class FormElement_Textinput extends FormElement
{
  public $type = "Text";
  public $value = "";
}

class FormElement_Number extends FormElement
{
  public $type = "Number";
  public $value = 0;
}

class FormElement_Date extends FormElement
{
  public $type = "Date";
  public $value = "";
}


class SelectOption
{
  public $label = ""; //the visible text
  public $value = ""; //the value
  public $selected = false; //if true, then the HTML selected attribute will be set
}

class FormElement_Select extends FormElement
{
  public $type = "Select";
  public $value = []; //value is an array of SelectOption instances
}

class FormElement_Checkbox extends FormElement
{
  public $type = "Checkbox";
  public $checked = false; //if set to true the checkbox is checked
  public $value = "";
}


// friendica specific form elements:

class FormElement_ThemeSelect extends FormElement_Select
{
  public $type = "ThemeSelect";
}

class FormElement_ACLSelect extends FormElement
{
  public $type = "ACLSelect";
}

class FormElement_Textarea extends FormElement
{
  public $type = "Textarea";
  public $value = "";
}

/// A FormattedTextarea has formatting buttons attached to it if JavaScript is enabled
class FormElement_FormattedTextarea extends FormElement_Textarea
{
  public $type = "FormattedTextarea";
}

?>