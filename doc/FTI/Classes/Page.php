<?php

include ('./Widget.php');

/// page is a collection of all the content of the page
class Page
{
  public $title = ""; //a string for a headline in the main content (top of <section> content)
  public $description = ""; //some text below the headline
  public $sideWidgets = []; //array of Widget instances
  public $widgets = []; //array of Widget instances
  public $popups = []; //array of Widget instances (which are displayed as popup)
}

?>