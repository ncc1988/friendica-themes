## FTI - Friendica theme interface

This is a proposal for a standardised theme interface for Friendica.
The idea is that all objects that are passed to templates are defined in classes and that these classes allow maximum flexibility on where the objects are drawn on a page. 

Benefits:

1. Template code has to be written only once for an object => no code duplication and a lot less templates. Plus, theme creation becomes easier since you just have to define less than 10 templates in your theme. With the new interface you could draw the nav bar, the settings pages, the new post form and other stuff by just using four templates (the Saar theme does this after reorganising the objects from other templates).

2. All HTML code generation can be done by the templates. Furthermore one theme engineer might also decide to use SVG or something elese instead of HTML in his theme. This would also be possible with the new interface.

3. Friendica plugins can be agnostic to themes and they don't need to draw their own HTML code. They just have to build a widget or a page from the classes for the template interface and pass this to the function that calls the base template for a page. Styling and generating HTML can be done by themes and their templates. __So in short: Plugins would integrate well in any theme.__

f course this new interface would be incompatible with existing themes but the old theme interface could be used along with the new one for backward compatibility with existing themes. The theme interface could be selected by checking in PHP code if the file FTI.tpl exists. If it does then the new theme interface is used. Otherwise the old interface is used.

The classes are defined in the Classes subdirectory.