<?php

//(this was heavily inspired from the "vier" theme)

$uid = get_theme_uid();

//first output the universal style attributes:
if(file_exists($THEMEPATH.'/style.css'))
{
  echo file_get_contents($THEMEPATH.'/style.css');
}

// then the configurable style attributes:

//whether round corners shall be used or not:

$useRoundCorners = get_pconfig(local_user(), 'Saar', 'useRoundCorners');
if($useRoundCorners == "")
{
  $useRoundCorners = get_config('Saar', 'useRoundCorners');
  if($useRoundCorners == "")
  {
    $useRoundCorners = true;
  }
}

if($useRoundCorners == false)
{
  echo
".RoundCorners, *
{
  border-radius:0em;
}
";
}
else
{
  /*
    In the future, the border-radius could be configurable:
  $radius = get_pconfig($uid, 'Saar', 'radius');
  if($radius != "")
  {
    echo 
".RoundCorners
{
  border-radius:".$radius."em;
}
"  
  }
  else
  {
    echo 
".RoundCorners
{
  border-radius:0.5em;
}
"
  
  }
 */ 
       
}

//then output the selected color scheme (or the default if none selected):

$colorScheme = get_pconfig($uid, 'Saar', 'colorScheme');

if($colorScheme == "")
{
  //no style set:
  $colorScheme = get_config('Saar', 'colorScheme');
  if($colorScheme == "")
  {
    //still no style set: use default color scheme:
    $colorScheme = "Bostalsee";
  }
}



switch($colorScheme)
{
  case "Bostalsee":
  {
    echo file_get_contents($THEMEPATH.'/ColorSchemes/Bostalsee.css');
    break;
  }
  case "Voelklingen":
  {
    echo file_get_contents($THEMEPATH.'/ColorSchemes/Voelklingen.css');
    break;
  }
  case "Waldhoelzbach":
  {
    echo file_get_contents($THEMEPATH.'/ColorSchemes/Waldhoelzbach.css');
    break;
  }
  case "Terminal":
  {
    echo file_get_contents($THEMEPATH.'/ColorSchemes/Terminal.css');
    break;
  }
  case "Development":
  {
    echo file_get_contents($THEMEPATH.'/ColorSchemes/Development.css');
    break;
  }
}


?>