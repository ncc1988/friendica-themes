<form 
  action="{{$form.action}}" method="{{$form.method}}"
  {{if $form.name}}
  name="{{$form.name}}"
  {{/if}}
  >
{{foreach $form.elements as $element}}
  {{if $element.type == "formattedTextarea"}}
    {{include file="FTI_FormattedTextarea.tpl" element=$element}}
  {{elseif $element.type == "themeSelect"}}
  <div class="FieldRow">
    <div>
      <div class="FieldCell">{{$element.title|escape:'html'}}</div>
      <div class="FieldCell">
        <select class="Field Border RoundCorners {{if $element.fullWidth}}FullWidth{{/if}}" name="{{$element.name|escape:'html'}}"
          {{if $element.jsEvents}}
            {{foreach $element.jsEvents as $jsevent}}
              {{$jsevent.event}}="{{$jsevent.action}}"
            {{/foreach}}
          
          {{/if}}
          >
          {{foreach $element.option as $o}}
          <option value="{{$o.value|escape:'html'}}" {{if $o.selected}}selected="selected"{{/if}}>{{$o.value|escape:'html'}}</option>
          {{/foreach}}
        </select>
        <div class="FieldHint">{{$element.description|escape:'html'}}</div>
      </div>
    </div>
    <div>  
      <div class="FieldCell">
        <img class="BigIcon Border RoundCorners ThemePreview" style="display:block;" />
      </div>
      <div class="FieldCell SmallText">
        <div class="ThemePreviewField" ></div>
        <div class="ThemePreviewField" ></div>
        <div class="ThemePreviewField" ></div>
      </div>
    </div>
  </div>
  {{elseif $element.type == "aclSelector"}}
  <div class="FieldRow JS">
    <input type="hidden" name="{{$element.name|escape:'html'}}" value="{{$element.value|escape:'html'}}" class="ACLTarget" />
    <img class="Button RoundCorners SmallIcon SmallText" src="images/icons/unlock.png" alt="{{$element.description|escape:'html'}}"
          onclick="javascript:void(FriendicaUI.Popup.Open(this.parentNode, 'ACLSelector'));"
          title="{{$element.description|escape:'html'}}"></img>
  </div>
  {{elseif $element.type == "textarea"}}
  <div class="FieldRow">
    <textarea name="{{$element.name|escape:'html'}}" class="Textarea InputField RoundCorners {{if $element.fullWidth}}FullWidth{{/if}}">{{$element.value|escape:'html'}}</textarea>
  </div>
  {{elseif $element.type == "select"}}
  <div class="FieldRow">
    {{if $element.title}}
    <div class="FieldCell FieldTitle">{{$element.title|escape:'html'}}</div>
    {{/if}}
    <div class="FieldCell">
      <select name="{{$element.name|escape:'html'}}" class="InputField RoundCorners {{if $element.fullWidth}}FullWidth{{/if}}">
        {{foreach $element.option as $option}}
        <option value="{{$option.value|escape:'html'}}" {{if $option.selected}}selected="selected"{{/if}}>
          {{$option.label|escape:'html'}}
        </option>
        {{/foreach}}
      </select>
      {{if $element.description}}
      <div class="FieldDescription SmallText">{{$element.description|escape:'html'}}</div>
      {{/if}}
    </div>
  </div>
  {{elseif $element.type == "checkbox"}}
  <div class="FieldRow">
    {{if $element.title}}
      <div class="FieldCell FieldTitle">{{$element.title|escape:'html'}}</div>
    {{/if}}
    <div class="FieldCell">
      <input type="checkbox" name="{{$element.name|escape:'html'}}" value="1"
        {{if $element.value == 1}}
          checked="checked"
        {{/if}}
        {{if $element.autofocus}}
          autofocus="autofocus"
        {{/if}}
        {{if $element.required}}
          required="required"
        {{/if}}
      />
      <label for="{{$element.name}}" class="Border RoundCorners"></label>
      {{if $element.description}}
        <div class="FieldDescription SmallText">{{$element.description|escape:'html'}}</div>
      {{/if}}
    </div>
  </div>
  {{elseif $element.type == "rawfield"}}
  <div class="FieldRow">
    {{if $element.title}}
    <div class="FieldCell FieldTitle">{{$element.title|escape:'html'}}</div>
    {{/if}}
    <div class="FieldCell">
      {{$element.content}}
      {{if $element.description}}
        <div class="FieldDescription SmallText">{{$element.description|escape:'html'}}</div>
      {{/if}}
    </div>
  </div>
  {{elseif $element.type == "hidden"}}
  <input name="{{$element.name|escape:'html'}}" type="{{$element.type|escape:'html'}}" 
        class="InputField RoundCorners {{if $element.fullWidth}}FullWidth{{/if}}" 
        value="{{$element.value|escape:'html'}}"
        {{if $element.placeholder}}
           placeholder="{{$element.placeholder|escape:'html'}}"
        {{/if}}
        {{if $element.selected}}
          selected="selected"
        {{/if}}
        {{if $element.autofocus}}
          autofocus="autofocus"
        {{/if}}
        {{if $element.required}}
          required="required"
        {{/if}}
      />
  {{else}}
  <div class="FieldRow">
    {{if $element.title}}
    <div class="FieldCell FieldTitle">{{$element.title|escape:'html'}}</div>
    {{/if}}
    <div class="FieldCell">
      <input name="{{$element.name|escape:'html'}}" type="{{$element.type|escape:'html'}}" 
        class="InputField Border RoundCorners {{if $element.fullWidth}}FullWidth{{/if}}" 
        value="{{$element.value|escape:'html'}}"
        {{if $element.placeholder}}
            placeholder="{{$element.placeholder|escape:'html'}}"
        {{/if}}
        {{if $element.selected}}
          selected="selected"
        {{/if}}
        {{if $element.autofocus}}
          autofocus="autofocus"
        {{/if}}
        {{if $element.required}}
          required="required"
        {{/if}}
      />
    {{if $element.description}}
      <div class="FieldDescription SmallText">{{$element.description|escape:'html'}}</div>
    {{/if}}
    </div>
  </div>
  {{/if}}
{{/foreach}}
  <div class="Center">
    <input class="Button RoundCorners FullWidth" type="submit"
      {{if $form.specialSubmitName}}name="$form.specialSubmitName|escape:'html'"{{else}}name="submit"{{/if}}
      value="{{$form.submitValue|escape:'html'}}" />
  </div>
</form>
