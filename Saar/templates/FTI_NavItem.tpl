<span
  {{if $navItem.type == "menu"}}
    class="NavItem MenuActivator">
    <a rel="#{{$navItem.name}}">
      {{if $navItem.image}}
        <img class="SmallIcon RoundCorners" src="{{$navItem.image.source|escape:'html'}}" alt="{{$navItem.image.description|escape:'html'}}"></img>
      {{else}}
      {{$navItem.title|escape:'html'}}
      {{/if}}
    </a>
    <ul id="{{$navItem.name}}" class="Menu">
      {{foreach $navItem.content as $entry}}
      <li><a class="MenuEntry" href="{{$entry.action|escape:'html'}}">{{$entry.title|escape:'html'}}</a></li>
      {{/foreach}}
    </ul>
  {{elseif $navItem.type == "link"}}
    class="NavItem">
    <a href="{{$navItem.content.action|escape:'html'}}">{{$navItem.content.title|escape:'html'}}</a>
  {{elseif $navItem.type == "spacer"}}
    class="NavItem NavSpacer">&nbsp;
  {{elseif $navItem.type == "text"}}
    {{if $navItem.content.size == "big"}}
      class="NavItem BigText">
    {{elseif $navItem.content.size == "small"}}
      class="NavItem SmallText">
    {{else}}
      class="NavItem">
    {{/if}}
    {{$navItem.content.text|escape:'html'}}
  {{elseif $navItem.type == "html"}}
    class="NavItem">
    {{$navItem.content}}
  {{/if}}
</span>