<div
{{if $pageBlock.type == "title" }}
  {{if $pageBlock.layer == 1}}
    class="PageBlock Layer1">
    <h1>{{$pageBlock.title}}</h1>
  {{elseif $pageBlock.layer == 2}}
    class="PageBlock Layer2">
    <h2>{{$pageBlock.title}}</h2>
  {{else}}
    class="PageBlock Layer3">
    <h3>{{$pageBlock.title}}</h3>
  {{/if}}
  
  {{if $pageBlock.description}}
    <p>{{$pageBlock.description}}</p>
  {{/if}}
{{elseif $pageBlock.type == "form"}}
  {{if $pageBlock.title}}
    {{if $pageBlock.layer == 1}}
      class="PageBlock Border RoundCorners Layer1">
      <h1>{{$pageBlock.title}}</h1>
    {{elseif $pageBlock.layer == 2}}
      class="PageBlock Border RoundCorners Layer2">
      <h2>{{$pageBlock.title}}</h2>
    {{else}}
      class="PageBlock Border RoundCorners Layer3">
      <h3>{{$pageBlock.title}}</h3>
    {{/if}}
  {{/if}}
  {{include file="FTI_Form.tpl" form=$pageBlock.content}}
{{/if}}
</div>