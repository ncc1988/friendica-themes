<div class="FieldRow">
  <div class="FieldCell">{{$field.1}}</div>
  <div class="FieldCell">
    <select class="Field Border RoundCorners" name="{{$field.0}}"
      {{if $field.5}}
      onchange="javascript:void(FriendicaUI.Settings.PreviewTheme(this, 'Settings_Display_Preview'));">
      {{/if}}
      {{foreach $field.4 as $opt=>$val}}
      <option value="{{$opt|escape:'html'}}" {{if $opt==$field.2}}selected="selected"{{/if}}>{{$val}}</option>
      {{/foreach}}
    </select>
    <div class="FieldHint">{{$field.3}}</div>
  </div>
</div>
{{if $field.5}}
<div class="FieldRow" id="Settings_Display_Preview" style="min-height:10em;height:10em;">
  <div class="FieldCell">
    <img class="BigIcon Border RoundCorners" style="display:block;" />
  </div>
  <div class="FieldCell SmallText">
    <div class="ThemePreviewField" ></div>
    <div class="ThemePreviewField" ></div>
    <div class="ThemePreviewField" ></div>
  </div>
</div>
{{/if}}