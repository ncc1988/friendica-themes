<div class="FieldRow">
  <div class="FieldCell">{{$field.1}}</div>
  <div class="FieldCell">
    <select class="Field Border RoundCorners" name="{{$field.0}}"
      {{if $field.4 eq 'required'}} required="required"{{/if}}
      {{if $field.5 eq 'autofocus'}} autofocus="autofocus"{{/if}} >
      {{foreach $field.4 as $opt=>$val}}
      <option value="{{$opt|escape:'html'}}" {{if $opt==$field.2}}selected="selected"{{/if}}>{{$val}}</option>
      {{/foreach}}
    </select>
    <div class="FieldHint">{{$field.3}}</div>
  </div>
</div>