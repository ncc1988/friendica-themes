{{$elements = []}}
{{$elements[] = ["type" => "html", "content" => $banner]}}

{{if $userinfo}}
  {{if $nav.login}}
    {{$elements[] = ["type" => "link", "content" => ["action" => $nav.login.0, "title" => $nav.login.1]]}}
  {{/if}}
  {{$userMenu = ["type" => "menu", "name" => "UserMenu",
    "image" => ["source" => $userinfo.icon, "description" => $userinfo.name],
    "content" => []]
  }}
  {{if $nav.notifications}}
    {{$userMenu.content[] = ["action" => $nav.notifications.0, "title" => $nav.notifications.1]}}
  {{/if}}
  {{if $nav.contacts}}
    {{$userMenu.content[] = ["action" => $nav.contacts.0, "title" => $nav.contacts.1]}}
  {{/if}}
  {{foreach $nav.usermenu as $entry}}
    {{$userMenu.content[] = ["action" => $entry.0, "title" => $entry.1]}}
  {{/foreach}}
  {{if $nav.home}}
    {{$userMenu.content[] = ["action" => $nav.home.0, "title" => $nav.home.1]}}
  {{/if}}
  {{$elements[] = $userMenu}}
{{/if}}

{{if $nav.network}}
  {{$elements[] = [ "type" => "link", "content" => ["action" => $nav.network.0, "title" => $nav.network.1]]}}
{{/if}}

{{if $nav.community}}
  {{$elements[] = [ "type" => "link", "content" => ["action" => $nav.community.0, "title" => $nav.community.1]]}}
{{/if}}

{{$elements[] = ["type" => "spacer", "content" => []]}}


{{$settingsMenu = ["type" => "menu", "name" => "SettingsMenu",
    "image" => ["source" => "./view/theme/Saar/icons/Settings.svg", "description" => $nav.settings.1],
    "content" => []]}}
{{if $nav.profiles}}
  {{$settingsMenu.content[] = ["action" => $nav.profiles.0, "title" => $nav.profiles.1]}}
{{/if}}
{{if $nav.settings}}
  {{$settingsMenu.content[] = ["action" => $nav.settings.0, "title" => $nav.settings.1]}}
{{/if}}
{{if $nav.apps}}
  {{$settingsMenu.content[] = ["action" => $nav.apps.0, "title" => $nav.apps.1]}}
{{/if}}
{{if $nav.admin}}
  {{$settingsMenu.content[] = ["action" => $nav.admin.0, "title" => $nav.admin.1]}}
{{/if}}
{{if $nav.help}}
  {{$settingsMenu.content[] = ["action" => $nav.help.0, "title" => $nav.help.1]}}
{{/if}}
{{if $nav.logout}}
  {{$settingsMenu.content[] = ["action" => $nav.logout.0, "title" => $nav.logout.1]}}
{{/if}}
{{$elements[] = $settingsMenu}}

{{$elements[] = ["type" => "text", "content" => ["size" => "small", "text" => $sitelocation]]}}


<nav>
  <div id="PopupBackground" onclick="javascript:void(FriendicaUI.Popup.Close());" data-Popup=""><!-- this is here because the nav bar is always loaded. TODO: move it directly after body element! --></div>
  {{foreach $elements as $e}}
    {{include file="FTI_NavItem.tpl" navItem=$e}}
  {{/foreach}}
</nav>