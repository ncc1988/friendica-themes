{{if $inv}}
{{$buttons[] = ["action" => "invite", "title" => $inv]}}
{{/if}}
{{$buttons[] = ["action" => "match", "title" => $similar]}}
{{$buttons[] = ["action" => "suggest", "title" => $suggest]}}
{{$buttons[] = ["action" => "randprof", "title" => $random]}}
{{$form = ["method" => "post", "action" => "dirfind",
    "elements" => [["type" => "text", "name" => "search", "title" => $hint]],
    "submitValue" => $findthem]}}
{{$widget = ["title" => $findpeople, "parts" => [
    ["type" => "buttons", "content" => $buttons],
    ["type" => "form", "content" => $form]
    ]
  ]}}
{{include file="Widget.tpl" widget=$widget}}
