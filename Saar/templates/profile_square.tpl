<div class="ProfileCard">
  <a href="{{$url}}" class="ProfileCardLeft">
    <img class="ProfileCardImage" src="{{$photo}}" alt="{{$name}}" title="{{$name}} [{{$tags}}]"></img>
  </a>
  <div class="ProfileCardRight">
    <a class="ProfileCardName" href="{{$url}}" title={{$name}} [{{$tags}}]">{{$name}}</a>
    {{if $ignore}}
    <a class="ProfileCardSubtitle Button RoundCorners" href="{{$ignoreLink}}">{{$ignore}}</a>
    {{/if}}
    {{if $connLink}}
    <a class="ProfileCardSubtitle Button RoundCorners" href="{{$connLink}}" title="{{$connText}}">{{$connText}}</a>
    {{/if}}
  </div>
</div>