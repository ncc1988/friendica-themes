<div class="MenuActivator Float Box SmallText RoundCorners Padding Margin Layer2">
  <div class="Float">
    <a href="{{$contact.url}}" title="{{$contact.img_hover}}">
      <img class="Icon RoundCorners" src="{{$contact.thumb}}" alt="{{$contact.name}}" />
    </a>
  </div>
  <div class="Bold">{{$contact.name}}</div>
  {{if $contact.photo_menu}}
  <ul class="Border Menu">
    {{foreach $contact.photo_menu as $c}}
      {{if $c.2}}
      <li><a class="Link" target="redir" href="{{$c.1}}">{{$c.0}}</a></li>
      {{else}}
      <li><a class="Link" href="{{$c.1}}">{{$c.0}}</a></li>
      {{/if}}
    {{/foreach}}
  </ul>
  {{/if}}
</div>