{{$buttons = []}}
{{foreach $groups as $group}}
{{$buttons[] = ["type" => "button", "content" => ["action" => $group.href, "title" => $group.text, "selected" => $group.selected]]}}
{{/foreach}}
{{$widget = ["title" => $title, "parts" => $buttons, "isSideWidget" => true]}}
{{include file="FTI_Widget.tpl" widget=$widget}}
