<div class="FieldRow">
  <div class="FieldCell">{{$field.1}}</div>
  <div class="FieldCell">
    <input class="Field Border RoundCorners" name="{{$field.0}}" value="{{$field.2|escape:'html'}}" />
    <div class="FieldHint">{{$field.3}}</div>
  </div>
</div>