{{if $pager}}
<div class="Margin Row">
  {{if $pager.first}}
  <a class="Button RoundCorners FloatL" href="{{$pager.first.url}}">{{$pager.first.text}}</a>
  {{/if}}
  {{if $pager.prev}}
  <a class="Button RoundCorners FloatL" href="{{$pager.prev.url}}">{{$pager.prev.text}}</a>
  {{/if}}
  
  {{foreach $pager.pages as $page}}
  <a class="Button RoundCorners FloatM" href="{{$page.url}}">{{$page.text}}</a>
  {{/foreach}}
  
  {{if $pager.last}}
  <a class="Button RoundCorners FloatR" href="{{$pager.last.url}}">{{$pager.last.text}}</a>
  {{/if}}
  
  {{if $pager.next}}
  <a class="Button RoundCorners FloatR" href="{{$pager.next.url}}">{{$pager.next.text}}</a>
  {{/if}}
</div>
{{/if}}