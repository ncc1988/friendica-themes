<h2>{{$invite}}</h2>

<form action="invite" method="post">
  <p>{{$addr_text}}
  <textarea class="Textarea RoundCorners Block FullWidth" name="recipients" rows="8" ></textarea>
  <p>{{$msg_text}}</p>
  <textarea class="Textarea RoundCorners Block FullWidth" name="message" rows="10">{{$default_message}}</textarea>
  <div class="Center">
    <input class="Button RoundCorners FullWidth" type="submit" name="submit" value="{{$submit|escape:'html'}}" />
  </div>
</form>