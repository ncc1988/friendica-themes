{{$widget = ["idattr" => "NewPost", "layer" => 1, "isSideWidget" => false, "parts" => []]}}
{{$form = ["name" => "NewPostForm", "method" => "post", "action" => $action, "submitValue" => $share]}}
{{$form.elements = [
  ["type" => "hidden", "name" => "type", "value" => $ptyp],
  ["type" => "hidden", "name" => "profile_uid", "value" => $profile_uid],
  ["type" => "hidden", "name" => "profile_name"],
  ["type" => "hidden", "name" => "return", "value" => $return_path],
  ["type" => "hidden", "name" => "location", "value" => $defloc],
  ["type" => "hidden", "name" => "coord", "value" => ""],
  ["type" => "hidden", "name" => "post_id", "value" => $post_id],
  ["type" => "hidden", "name" => "preview", "value" => "0"],
  ["type" => "hidden", "name" => "post_id_random", "value" => $rand_num]
]}}
{{if $placeholdercategory}}
  {{$form.elements[] = ["type" => "text", "name" => "title", "value" => $category, "placeholder" => $placeholdercategory]}}
{{/if}}
{{$form.elements[] = ["type" => "text", "name" => "title", "value" => $title, "placeholder" => $placeholdertitle]}}
{{$form.elements[] = ["type" => "formattedTextarea", "name" => "body", "value" => $content, "placeholder" => $share, "mediaAllowed" => true]}}
{{if $permset}}
  {{$form.elements[] = ["type" => "aclSelector", "name" => "contact_allow[]", "value" => $notes_cid, "description" => $permset]}}
{{/if}}
{{$formPart = ["type" => "form", "content" => $form, "layer" => 1]}}
{{$widget.parts[] = $formPart}}

{{include file="FTI_Widget.tpl" widget=$widget}}

<div class="Popup Background" id="Popup_NewPost_UploadImage">
    <div class="PopupActions">
      <img style="width:2em;height:2em;" src="images/icons/delete.png" onclick="javascript:void(FriendicaUI.Popup.Close('Popup_NewPost_UploadImage'));" alt="X"></img>
    </div>
    <h3>{{$upload|escape:'html'}}</h3>
    <div>
      <input id="Popup_NewPost_UploadImage_File" type="file" multiple="multiple" />
      <div class="FullWidth Button RoundCorners" onclick="javascript:void(FriendicaUI.NewPost.UploadImages());">{{$upload|escape:'html'}}</div>
    </div>
  </div>
</div>