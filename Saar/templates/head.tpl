<base href="{{$baseurl}}/" />
<link rel="stylesheet" type="text/css" href="{{$stylesheet}}" media="all" />
<!-- for plugin compatibility: -->
<script type="text/javascript" src="{{$baseurl}}/js/jquery.js" ></script>
<!-- our own JS code: -->
<script type="text/javascript" src="{{$baseurl}}/view/theme/Saar/js/FriendicaUI.js"></script>