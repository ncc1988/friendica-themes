
function FriendicaUI(){}

FriendicaUI.ACL = function(){};


FriendicaUI.ACL.RequestContactList = function()
{
  //first let's check if we're on a page that needs the ACL list:
  var aclResultNode = document.getElementById("ACLResults");
  if(aclResultNode == undefined)
  {
    //we're on another page: return
    return false;
  }
  
  var request = new XMLHttpRequest();
  
  request.onreadystatechange = function()
  {
    if(request.readyState == 4)
    {
      //got a response:
      FriendicaUI.ACL.DrawContactList(JSON.parse(request.responseText));
    }
  };
  
  request.open("POST", "acl", true);
  request.send(); //TODO: maybe limit contacts to 100, like the original JS code
};


FriendicaUI.ACL.DrawContactList = function(contactList)
{
  if(contactList == undefined)
  {
    return false;
  }
  
  //get the template:
  var template = document.getElementById("NewPost_ACLItem_Template");
  
  
  for (var i = 0; i < contactList.items.length; i++)
  {
    //draw each item
    var newItem = template.cloneNode(true);
    if(contactList.items[i].uids != undefined)
    {
      newItem.setAttribute("data-uids", contactList.items[i].uids.toString());
    }
    else
    {
      newItem.setAttribute("data-cid", contactList.items[i].id);
    }
    newItem.setAttribute("id", "NewPost_ACLItem_"+contactList.items[i].id);
    //TODO: special handling for groups
    
    var image = newItem.getElementsByTagName("img")[0];
    image.setAttribute("src", contactList.items[i].photo);
    
    var name = newItem.getElementsByClassName("ACLItem_Name")[0];
    name.innerHTML = contactList.items[i].name;
    
    //make the new item visible:
    document.getElementById("ACLResults").appendChild(newItem);
    newItem.classList.remove("Template");
    //set the onchange event for the input element:
    newItem.getElementsByClassName("ACLItem_Visibility")[0].onchange = 
      FriendicaUI.ACL.ToggleContactInclusion(
        newItem, "NewPost_ACLItem_"+contactList.items[i].id);
  }
};


FriendicaUI.ACL.ToggleContactInclusion = function(checkbox, aclItemID)
{
  var aclItem = document.getElementById(aclItemID);
  if(aclItem == undefined)
  {
    return false;
  }
  
  //TODO: special handling for groups!
  
  if(checkbox.checked)
  {
    aclItem.classList.add("SelectedItem");
    if(!(FriendicaUI.NewPost.visibleTo instanceof Array))
    {
      FriendicaUI.NewPost.visibleTo = [];
      if(aclItem.getAttribute("data-uids") != undefined)
      {
        FriendicaUI.NewPost.visibleTo.append(aclItem.getAttribute("data-uids"));
      }
      else
      {
        FriendicaUI.NewPost.visibleTo.append(aclItem.getAttribute("data-cid"));
      }
    }
  }
  else
  {
    aclItem.classList.remove("SelectedItem");
    if(FriendicaUI.NewPost.visibleTo instanceof Array)
    {
      var index = -1;
      if(aclItem.getAttribute("data-uids") != undefined)
      {
        index = FriendicaUI.NewPost.visibleTo.indexOf(aclItem.getAttribute("data-uids"));
      }
      else
      {
        index = FriendicaUI.NewPost.visibleTo.indexOf(aclItem.getAttribute("data-cid"));
      }
      FriendicaUI.NewPost.visibleTo.splice(index, 1);
    }
  }
};


FriendicaUI.ACL.SelectAll = function()
{
  FriendicaUI.NewPost.visibleTo = true;
  
  //TODO: uncheck checked checkboxes and make the selected elements unselected
};


FriendicaUI.ACL.Apply = function()
{
  //TODO: REMOVE some stuff: FriendicaUI.ACL.ToggleContactInclusion() does the same up to some point
  
  //first we have to get all the CIDs
  var aclResults = document.getElementById("ACLResults").getElementsByClassName("ACLItem");
  var cidList = [];
  for (var i = 0; i < aclResults.length; i++)
  {
    var checkbox = aclResults[i].getElementsByClassName("ACLItem_Visibility_Checkbox")[0];
    if(checkbox.checked)
    {
      if(aclResults[i].getAttribute("data-uids") != undefined)
      {
        cidList.push(aclResults[i].getAttribute("data-uids"));
      }
      else
      {
        cidList.push(aclResults[i].getAttribute("data-cid"));
      }
      
    }
  }
  
  //now we create a special input field (if not present)
  var cidField = document.getElementById("NewPost_CidField");
  if(cidField == undefined)
  {
    var newCidField = document.createElement("input");
    newCidField.setAttribute("id", "NewPost_CidField");
    newCidField.setAttribute("type", "hidden");
    newCidField.setAttribute("name", "contact_allow[]");
    document.forms["NewPostForm"].appendChild(newCidField);
    cidField = newCidField;
  }
  
  cidField.setAttribute("value", cidList.toString());
  
  //close the popup if everything went fine
  FriendicaUI.Popup.Close();
};


FriendicaUI.FormattedText = function(){};

FriendicaUI.FormattedText.InsertBBCode = function(baseNode, bbcode)
{
  if(baseNode == undefined)
  {
    return false;
  }
  if(bbcode == undefined)
  {
    return false;
  }
  
  //find the new post textarea:
  var textarea = baseNode.getElementsByClassName("FormattedTextarea")[0]; //this is unique for every formattedTextarea "input field"
  if(textarea == undefined)
  {
    return false;
  }
  
  //first strip the bbcode from its (optional) parameters:
  bbparam = bbcode.split("=")[1];
  bbcode = bbcode.split("=")[0];
  
  //get the cursor position:
  
  var text = undefined;
  var afterText = undefined;
  var newCursor = 0;
  
  if(textarea.selectionStart != textarea.selectionEnd)
  {
    //some text is selected: copy it and wrap the bbcode around it:
    text = textarea.value.slice(textarea.selectionStart, textarea.selectionEnd);
    afterText = textarea.value.substr(textarea.selectionEnd);
    textarea.value = textarea.value.slice(0, textarea.selectionStart);
    newCursor = textarea.selectionStart + 5 + text.length + (bbcode.length*2); //[bbcode] + text + [/bbcode]
  }
  else
  {
    //no text selected:
    text = "";
    afterText = textarea.value.substr(textarea.selectionStart, textarea.value.length);
    textarea.value = textarea.value.slice(0, textarea.selectionStart);
    newCursor = textarea.selectionStart + 2 + bbcode.length; //the bbcode and the characters "[]"
  }
  
  textarea.value = textarea.value += "["+bbcode;
  if(bbparam != undefined)
  {
    textarea.value += "="+bbparam;
    newCursor += bbparam.length + 1; //the string and the '=' character
  }
  textarea.value += "]"+text+"[/"+bbcode+"]"+afterText;
  
  //set the new selection start and set the focus:
  textarea.selectionStart = newCursor;
  textarea.selectionEnd = newCursor;
  textarea.focus();
};


FriendicaUI.FormattedText.InsertURL = function(baseNode)
{
  if(baseNode == undefined)
  {
    return false;
  }
  
  //find the textarea:
  var textarea = baseNode.getElementsByClassName("FormattedTextarea")[0];
  if(textarea == undefined)
  {
    return false;
  }
  
  //get the cursor position:
  
  var text = undefined;
  var afterText = undefined;
  var newCursor = 0;
  
  if(textarea.selectionStart != textarea.selectionEnd)
  {
    //some text is selected: probably the URL:
    text = textarea.value.slice(textarea.selectionStart, textarea.selectionEnd);
    afterText = textarea.value.substr(textarea.selectionEnd);
    textarea.value = textarea.value.slice(0, textarea.selectionStart);
    newCursor = textarea.selectionStart + 12 + (text.length*2); //[url= + text] + [/url]
  }
  else
  {
    //no text selected: TODO: show URL popup
    text = "";
    afterText = textarea.value.substr(textarea.selectionStart, textarea.value.length);
    textarea.value = textarea.value.slice(0, textarea.selectionStart);
    newCursor = textarea.selectionStart + 6 + text.length; //[url= + text + ]
  }
  
  textarea.value += "[url="+text+"]"+text+"[/url]"+afterText;
  //set the new selection start and set the focus:
  textarea.selectionStart = newCursor;
  textarea.selectionEnd = newCursor;
  textarea.focus();
};


FriendicaUI.FormattedText.InsertImageURL = function(baseNode)
{
  if(baseNode == undefined)
  {
    return false;
  }
  
  //find the textarea:
  var textarea = baseNode.getElementsByClassName("FormattedTextarea")[0];
  if(textarea == undefined)
  {
    return false;
  }
  
  //get the cursor position:
  
  var text = undefined;
  var afterText = undefined;
  var newCursor = 0;
  
  if(textarea.selectionStart != textarea.selectionEnd)
  {
    //some text is selected: probably the URL:
    text = textarea.value.slice(textarea.selectionStart, textarea.selectionEnd);
    afterText = textarea.value.substr(textarea.selectionEnd);
    textarea.value = textarea.value.slice(0, textarea.selectionStart);
    newCursor = textarea.selectionStart + 11 + text.length; //[img] + text + [/img]
  }
  else
  {
    //no text selected: TODO: show URL popup
    text = "";
    afterText = textarea.value.substr(textarea.selectionStart, textarea.value.length);
    textarea.value = textarea.value.slice(0, textarea.selectionStart);
    newCursor = textarea.selectionStart + 5 + text.length; //[img]
  }
  
  textarea.value += "[img]"+text+"[/img]"+afterText;
  //set the new selection start and set the focus:
  textarea.selectionStart = newCursor;
  textarea.selectionEnd = newCursor;
  textarea.focus();
};


FriendicaUI.FormattedText.InsertText = function(baseNode, text)
{
  //inserts text at the current cursor position
  
  if(baseNode == undefined)
  {
    return false;
  }
  if(text == undefined)
  {
    return false;
  }
  
  //find the textarea:
  var textarea = baseNode.getElementsByClassName("FormattedTextarea");
  if(textarea == undefined)
  {
    return false;
  }
  
  var afterText = textarea.value.substr(textarea.selectionStart, textarea.value.length);
  textarea.value = textarea.value.slice(0, textarea.selectionStart);
  textarea.value += text + afterText;
  
  //deselect text and place the cursor after the inserted text:
  textarea.selectionStart += text;
  textarea.selectionEnd = textarea.selectionStart;
};


FriendicaUI.NewPost = function(){};

FriendicaUI.NewPost.visibleTo = true; //true: visible to all, otherwise: array with contact-IDs, otherwise:invalid







FriendicaUI.NewPost.InsertLocation = function()
{
  //TODO: implement HTML5 geolocation
};

FriendicaUI.NewPost.SetPermissions = function(apply)
{
  //set default value if not set:
  if(apply == undefined)
  {
    apply = false;
  }
  
  if(apply == false)
  {
    document.getElementById("NewPost_ACL").style.display = "block";
  }
  else
  {
    document.getElementById("NewPost_ACL").style.display = "none";
  }
};


FriendicaUI.NewPost.UploadImages = function()
{
  //first we have to get the file:
  var fileInput = document.getElementById("Popup_NewPost_UploadImage_File");
  if(fileInput == undefined)
  {
    return false;
  }
  
  //we may upload several images:
  var files = fileInput.files;
  for (var f of files)
  {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function()
      {
        if(request.readyState == 4)
        {
          FriendicaUI.NewPost.InsertText(request.responseText);
        }
      };
    request.open("POST", "wall_upload/"+FriendicaUI.userID, true);
    request.send("userfile="+f);
  }
  
  //all images were sent: clear the file list:
  fileInput.value = "";
};




FriendicaUI.Post = function(){};


FriendicaUI.Post.Mark = function(postID, verb)
{
  //mark the post with a verb: like, dislike etc.
  var request = new XMLHttpRequest();
  
  request.onreadystatechange = function()
  {
    if(request.readyState == 4)
    {
      //got a response:
      FriendicaUI.Post.UpdateMarkStatus(postID, 1);
    }
  };
  request.open("GET", "like/"+postID.toString()+"?verb="+verb.toString(), true);
  request.send();
};


FriendicaUI.Post.Like = function(postID)
{
  var request = new XMLHttpRequest();
  
  request.onreadystatechange = function()
  {
    if(request.readyState == 4)
    {
      //got a response:
      FriendicaUI.Post.UpdateLikeStatus(postID, 1);
    }
  };
  request.open("GET", "like/"+postID.toString(), true);
  request.send("verb=like");
};


FriendicaUI.Post.Dislike = function(postID)
{
  var request = new XMLHttpRequest();
  
  request.onreadystatechange = function()
  {
    if(request.readyState == 4)
    {
      //got a response:
      FriendicaUI.Post.UpdateMarkStatus(postID, -1);
    }
  };
  
  request.open("GET", "like/"+postID.toString(), true);
  request.send("verb=dislike");
};


FriendicaUI.Post.UpdateMarkStatus = function(postID, like)
{
  if(like == undefined)
  {
    like = 1;
  }
  //TODO: show the like without reloading!
  window.location = "#Post-"+postID.toString();
};


FriendicaUI.Post.Reshare = function(postID)
{
  window.alert("Resharing is not yet implemented!");
};


FriendicaUI.Post.SendNewComment = function(form, postID)
{
  if(postID == undefined)
  {
    //TODO: Debug message
    return false;
  }
  
  //collect data and submit it as HTTP(S) POST
  
  var request = new XMLHttpRequest();
  
  request.onreadystatechange = function()
  {
    if(request.readyState == 4)
    {
      //got a response:
      FriendicaUI.Post.ShowNewComment(request.responseText, postID);
    }
  };
  
  request.open("POST", form.action, true);
  request.send(new FormData(form));
  //console.log("Send comment via AJAX!");
};


FriendicaUI.Post.ShowNewComment = function(responseText, postID)
{
  //show comment
  if(postID == undefined)
  {
    //TODO: Debug message
    FriendicaUI.PrintError("ShowNewComment", "postID == undefined!");
    return false;
  }
  
  //parse response text (attached to this object):
  //console.log("responseText="+responseText);
  var response = JSON.parse(responseText);
  // response should be {"success":1}
  
  if(response.success == 1)
  {
    //everything went fine
    
    //reload the page at the comment (not nice, but simple):
    window.location.href="#Post-"+postID;
  }
};


FriendicaUI.Popup = function(){};

FriendicaUI.Popup.Close = function()
{
  var background = document.getElementById("PopupBackground");
  var popupName = background.getAttribute("data-Popup");
  if(popupName != undefined)
  {
    var popupNode = document.getElementById("Popup_"+popupName);
    if(popupNode != undefined)
    {
      popupNode.style.visibility = "hidden";
      popupNode.style.opacity = 0;
    }
  }
  background.style.visibility = "hidden";
  background.style.opacity = 0;
};


FriendicaUI.Popup.Open = function(popupName)
{
  if(popupName != undefined)
  {
    var background = document.getElementById("PopupBackground");
    var popupNode = document.getElementById("Popup_"+popupName);
    
    if(popupNode != undefined)
    {
      popupNode.style.visibility = "visible";
      popupNode.style.opacity = 1;
      background.style.visibility = "visible";
      background.style.opacity = 0.85;
      background.setAttribute("data-Popup",popupName);
    }
  }
};


// general functions

FriendicaUI.ShowField = function(field, displayStyle, preserveSpace)
{
  if(displayStyle == undefined)
  {
    displayStyle = "visible";
  }
  if(preserveSpace == undefined)
  {
    preserveSpace = true;
  }
  
  if(field != undefined)
  {
    if(preserveSpace == true)
    {
      field.style.visibility = displayStyle;
    }
    else
    {
      field.style.display = displayStyle;
    }
  }
};


FriendicaUI.HideField = function(fieldID)
{
  if(fieldID != undefined)
  {
    var field = document.getElementById(fieldID);
    if(field != undefined)
    {
      if(preserveSpace == true)
      {
        field.style.visibility = "hidden";
      }
      else
      {
        field.style.display = "none";
      }
    }
  }
};



FriendicaUI.PrintError = function(fnName, error)
{
  console.log("FriendicaUI error in "+fnName+": "+error);
};

FriendicaUI.ToggleMenu = function(elementID)
{
  var element = document.getElementById(elementID);
  if(element != undefined)
  {
    if(element.style.display != "block")
    {
      //menu is invisible: open it!
      element.style.display = "block";
    }
    else
    {
      //menu is visible: close it!
      element.style.display = "none";
    }
  }
};


FriendicaUI.ClearInput = function(sender)
{
  if(sender == undefined)
  {
    FriendicaUI.PrintError("ClearInput","sender is undefined!");
    return false;
  }
  switch(sender.tagName)
  {
    case "INPUT":
    {
      sender.value = "";
      break;
    }
    case "TEXTAREA":
    {
      sender.innerHTML = "";
      break;
    }
  }
  //TODO: make sure that the content of the input field is only cleared once! 
  //sender.removeAttribute("onclick");
};


FriendicaUI.CountChars = function(sender, target)
{
  //sender: the sending element (usually "this" in the HTML code)
  //targetID: the ID of the HTML element that will output the counted chars as text
  if(sender == undefined)
  {
    FriendicaUI.PrintError("CountChars","sender is undefined!");
    return false;
  }

  if(target == undefined)
  {
    FriendicaUI.PrintError("CountChars", "target node not found!");
    return false;
  }
  
  if((sender.tagName == "INPUT") || (sender.tagName == "TEXTAREA"))
  {
    //inputs and text areas have a value field whose chars we can count:
    target.innerHTML = sender.value.length;
  }
  
  return true;
};


FriendicaUI.ActivateJSElements = function()
{
  //we have to search all style sheets for the .JS rule
  for (var j = 0; j < document.styleSheets.length; j++)
  {
    for(var i = 0; i < document.styleSheets[j].cssRules.length; i++)
    {
      var rule = document.styleSheets[j].cssRules[i];
      //now we're looking at each rule:
      if(rule.selectorText == ".JS")
      {
        //we have found the rule!
        rule.style.display = ""; //delete the value
        //since there is nothing left to do we may return:
        return true;
      }
    }
  }
};


FriendicaUI.Calendar = function(){};

FriendicaUI.Calendar.Draw = function()
{
  var calendar = document.getElementById("Calendar");
  if(calendar == undefined)
  {
    //there is no calendar, so we don't have to draw it
    return false;
  }
  
  var cellTemplate = document.getElementById("Calendar_Cell_Template");
  
  
};


FriendicaUI.Settings = function(){};


FriendicaUI.Settings.PreviewTheme = function(caller)
{
  //caller is the calling HTML element
  //displayId is the ID of the img element wherein the preview image shall be displayed
  
  themeName = caller.value;
  if(themeName == undefined)
  {
    return false;
  }
  var themeSelectField = caller.parentNode.parentNode.parentNode;
  var img = themeSelectField.getElementsByClassName("ThemePreview")[0];
  if(img == undefined)
  {
    //why bother to send an AJAX request if the element to display the response it isn't there?
    return false;
  }
  
  //now request the theme image via AJAX:
  
  var request = new XMLHttpRequest();
  
  request.onreadystatechange = function()
  {
    if(request.readyState == 4)
    {
      //got a response:
      //parse JSON:
      var data = JSON.parse(request.responseText);
      
      //var img = previewContainer.getElementsByTagName("img")[0];
      img.setAttribute("src", data.img);
      img.setAttribute("alt", themeName);
      
      var textDivs = themeSelectField.getElementsByClassName("ThemePreviewField");
      textDivs[0].innerHTML = data.desc;
      
      textDivs[1].innerHTML = data.version;
      textDivs[2].innerHTML = data.credits;
    }
  };
  
  request.open("GET", "pretheme?theme="+themeName.toString(), true);
  request.send();
};



window.onload = function()
{
  FriendicaUI.ActivateJSElements();
  FriendicaUI.ACL.RequestContactList();
  
  FriendicaUI.Calendar.Draw();
  
  FriendicaUI.userID = profile_uid; //profile_uid is the uid of the logged in user
  
  //console.log("DEBUG: Saar theme JS code loaded!");
};


//the following lines are only there for not-yet-replaced orignal JavaScript code
//to prevent errors in the browsers's JS engine

function resizeIframe()
{
  return true;
}