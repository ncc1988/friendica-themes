<?php


/** 
  @function theme_content This function is responsible to load the theme's current configuration before showing it.
**/
function theme_content(&$a)
{
  if(!local_user())
  {
    //this probably is meant to prevent other users to modify something
    return;
  }
  
  //get_pconfig seems to be "get profile config"
  //get_config then would mean retrieving the server wide configuration
  
  $colorScheme = get_pconfig(local_user(), 'Saar', 'colorScheme');
  if($colorScheme == "")
  {
    $colorScheme = get_config('Saar', 'colorScheme');
    if($colorScheme == "")
    {
      $colorScheme = "Bostalsee";
    }
  }
  
  $useRoundCorners = get_pconfig(local_user(), 'Saar', 'useRoundCorners');
  if($useRoundCorners == "")
  {
    $useRoundCorners = get_config('Saar', 'useRoundCorners');
    if($useRoundCorners == "")
    {
      $useRoundCorners = true;
    }
  }
  
  
  return Saar_form($a, $colorScheme, $useRoundCorners);
}

/**
  @function theme_post theme_post handles POST requests for theme settings changes
**/
function theme_post(&$a)
{
  if(!local_user())
  {
    //a non-local user must not change theme settings!
    return;
  }
  
  if(isset($_POST['SaarSettingsSubmit']))
  {
    set_pconfig(local_user(), 'Saar', 'colorScheme', $_POST['Saar_colorScheme']);
    set_pconfig(local_user(), 'Saar', 'useRoundCorners', $_POST['Saar_useRoundCorners']);
  }
}


function Saar_form(&$a, $colorScheme, $useRoundCorners)
{
  $colorSchemes = array("Bostalsee" => "Bostalsee",
                        "Voelklingen" => "Voelklingen",
                        "Waldhoelzbach" => "Waldhoelzbach",
                        "Terminal" => "Terminal",
                        "Development" => "Development"
                      );
  /*
  $styleFiles = dir.glob($THEMEPATH.'/ColorSchemes/*.css');
  $styles = array();
  foreach ($styleFiles as $f)
  {
    array_push($styles, preg_replace('\/ColorScheme\/',"", preg_replace(".css$", "", $f)));
  }*/
  
  $t = get_markup_template("theme_settings.tpl");
  $o .= replace_macros(
          $t, 
          array('$submit' => t('Submit'),
                '$baseurl' => $a->get_baseurl(),
                '$title' => t('Theme settings'),
                '$colorScheme' => array('Saar_colorScheme', t('Set style'), $colorScheme, '', $colorSchemes),
                '$useRoundCorners' => array('Saar_useRoundCorners', t('Use round corners'), $useRoundCorners)
                )
          );
  return $o;
}


?>